package Inheritance2;

public class Animal {
	
	String name;
	
	public Animal(String name){
		this.name = name;
	}
	
	public void sleep() {
        System.out.println("An animal sleeps...");
    }
    
    public void eat() {

        System.out.println("An animal eats...");

    }


}
