package Inheritance2;

public class MainClass {

	public static void main(String[] args) {

		 Animal animal = new Animal("Animal");
	     Bird bird = new Bird("Bird");
	     Dog dog = new Dog("Dog");
	        
	     System.out.println();
	     animal.sleep();
	     animal.eat();

	     bird.sleep();
	     bird.eat();

	     dog.sleep();
	     dog.eat();

	}

}
