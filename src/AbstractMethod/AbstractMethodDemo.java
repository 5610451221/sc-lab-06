package AbstractMethod;

class AbstractMethodDemo
{
        public static void main(String args[]){
        	Square sObj = new Square(5,5);
            Rectangle rObj = new Rectangle(5,7);
            Circle cObj = new Circle(2);

            System.out.println("Area of square : " + sObj.area());
            System.out.println("Area of rectangle : " + rObj.area());
            System.out.println("Area of circle : " + cObj.area());
        }
}

