package AbstractMethod;

class Circle extends Shape
{
      float radius;

      Circle(float r){
    	  radius = r;
      }

      float area(){
    	  return Shape.pi * radius *radius;
      }
}