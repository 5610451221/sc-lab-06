package AbstractMethod;

class Rectangle extends Shape
{
      Rectangle(float h, float w){
    	  height = h;
          width = w;
      }

      float area(){
    	  return height * width;
      }
}