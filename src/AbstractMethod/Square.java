package AbstractMethod;

class Square extends Shape
{
      Square(float h, float w){
             height = h;
             width = w;
      }

      float area(){
    	  return height * width;
       }
}