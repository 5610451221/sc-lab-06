package AbstractMethod;

abstract class Shape
{
      public static float pi = (float) 3.142; 
      protected float height;
      protected float width;

      abstract float area();
}  
